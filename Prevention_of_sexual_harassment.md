# Prevention of Sexual Harassment

## What kinds of behaviour cause sexual harassment?

- Repeated compliments of an employee's appearance
- Commenting on the attractiveness of others in front of an employee
- Discussing one's sex life in front of an employee
- Asking an employee about his or her sex life
- Circulating nude photos or photos of women in bikinis or shirtless men in the workplace
- Making sexual jokes
- Sending sexually suggestive text messages or emails
- Leaving unwanted gifts of a sexual or romantic nature
- Spreading sexual rumors about an employee, or
- Repeated hugs or other unwanted touching (such as a hand on an employee's back).

## What would you do in case you face or witness any incident or repeated incidents of such behaviour?

- **Distract** – Interrupt the interaction.

This is the most creative and easy to utilize intervention styles. As a bystander, find a way to disrupt the situation. Some examples of how to do this are steering the conversation to another topic, asking the harasser a question that is unrelated to the situation or making a loud noise.


- **Delegate** – Ask someone else to help.

Finding someone else that is more equipped to handle the situation, such as a leader in your organization, can help with a resolution. The delegate intervention can be a powerful tool when a situation has escalated and there is a need for an authority figure to intervene.

- **Delay** – Check-in after the interaction.

You can always take action after a situation has occurred. Checking in with the person after an interaction, to see how they are feeling, is a great way to be a bystander. It shows that they are not alone in the situation, that they have an ally (that’s you!), and that they have a support system.

- **Document** – Keep a record of what you observe.

It’s important to put pen to paper, or fingers to keyboard, and document what you witnessed. Make sure to record the name of those involved in the situation, the date or dates that the behavior occurred, the time, the location of the occurrence, a description of the behavior, and details of what happened. Remember to be as specific as possible. Documenting the words that were said or the actions that were taken in detail.

