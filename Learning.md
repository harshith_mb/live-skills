### Question 1 - What is the Feynman Technique?


Feyman Technique is to take something that's hard to understand and try to clarify it in your mind by explaining it as if you were talking to a child.


### Question 2 - In Ted Talk [video](https://www.youtube.com/watch?v=O96fE1E-rf8) by Barbara Oakley, what was the most interesting story or idea for you?


1. **Memorization Technique**: Write down something you want to remember, then try to remember it without peeking at what you wrote. Doing this helps you get better at remembering things.

2. **Focused Work and Breaks**: Work hard for a bit, then take short breaks. This helps you stay productive and avoid getting too tired or bored.

3. **Avoid Passive Learning**: To really understand something, don't just read it like a robot. Get involved! Think about it, ask questions, maybe even write stuff down. This way, you'll really get what you're learning.

### Question 3 - What are active and diffused modes of thinking?

- **Active Focus Mode**

When our brain is in this mode, it's like it's on high alert. It focuses super hard on what we're doing, paying really close attention and getting totally involved in new tasks.


- **Diffused Focus Mode**

When our mind is in this state, it's like it's chilling out. It's relaxed and calm, which lets us think creatively and solve problems without feeling stressed out. It's a good vibe for coming up with cool ideas!

### Question 4 - According to the [video](https://www.youtube.com/watch?v=5MgBikgcWnY), what are the steps to take when approaching a new topic?

1. Deconstruct the skills
2. Learn enough to correct yourself
3. Remove barrier practice
4. Practice atleast for 20 hours while learning new skill.


