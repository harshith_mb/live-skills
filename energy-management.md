### Question-1 What are the activities you do that make you relax - Calm quadrant?

- Spending some time with friends.
- Sleep
- Eating favourite food.

### Question-2 When do you find getting into the Stress quadrant?

- While doing outcome obsessed activities.
- While travelling in a crowded transportation.

### Question-3: How do you understand if you are in the Excitement quadrant?

- Have a big smile in our facial expression.
- Do not have control on our own thoughts.

### Question-4 Paraphrase the Sleep is your Superpower video in your own words in brief.

- Sleep is crucial for learning and memory.
- A sleep less brain experiences a 40% deficit in the ability to make new memories.


### Question-5 What are some ideas that you can implement to sleep better?

- Spend less time on social media.
- Avoid caffines.
- Maintain a sleep routine.







