### Question 1 - What are the steps/strategies to do Active Listening?

1. Active Listening
2. Avoid getting distracted on your own thoughts.
3. Try not to Interrupt the speaker
4. Using door openers
5. Body language
6. Taking notes

### Question 2 - According to Fisher's model, what are the key points of Reflective Listening?

Reflective listening is basically used while showing empathy or sympathy towards someone or to engage emotionally with the target person by knowing their inner thoughts or feelings.

### Question 3 - What are the obstacles in your listening process?

- Diversion of thoughts
- Language barrier
- Fear of being judged
- Lack of attention
- Lack of interest

### Question 4 - What can you do to improve your listening?

- Pay attention to each detail
- Make notes when necessary
- Stop dreaming in my own world.
- Replying with door openers

### Question 5 - When do you switch to Passive communication style in your day to day life?

- When communicating with elderly person.
- While communicating with our parents.
- While dealing with senior officials of an organization you are working for.
- While accepting a serious mistake commited by you

### Question 6 - When do you switch into Aggressive communication styles in your day to day life?

Aggressive communication is not the right way to deal with the situation.

Prehaps, some time our emotions will not be in our control and may be aggressive to others. But its good practice to contral ourselves during such kind of situations.

### Question 7 - When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

- While dealing with know-it-all kind of person where you know the statement made by the person is not true.

### Question 8 - How can you make your communication assertive?

We can make our communication assertive by standing up for our rights while maintaining respect for the rights of others.

- We can implement assertive communication while dealing with colleague of our team when you have a better approach for a solution that he/she has before.

