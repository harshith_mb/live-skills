# Grit and Growth Mindset

### Question 1 - Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.

https://youtu.be/H14bBuluwB8?si=CW72Z4TfDRIWgZpc

Angela Duckworth, a psychologist, discusses her research on success and the importance of grit in achieving long-term goals. She explains IQ is important, it is not the sole predictor of success. Instead, grit, defined as passion and perseverance for long-term goals, is a significant predictor of success. 

### Question-2 Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.

https://www.youtube.com/watch?v=75GFzikmRY0

In this video, he diescribes the key differences between **GROWTH MINDSET and FIXED MINDSET** based on beliefs and focus.

| On| Growth Mindset| Fixed Mindset|
|-----------|---------------|--------------|
| BELIEFS | Skills are built, You can learn and grow| Skills are born, you cannot learn and grow |
| FOCUS |The process, Getting Better |  Performance outcomes, Not looking bad|

### Question-3 What is the Internal Locus of Control? What is the key point in the video?

Internal locus of control means that control comes from within. You have personal control over your own behavior.

#### Key points
- Do not worry of external factors which you think is affecting your goal.
- Try to appreciate tiny achievements you do.


### Question-4 What are the key points mentioned by speaker to build growth mindset 

1. Belief in ability to figure things out is the foundation of a growth mindset.
2. Question assumptions to overcome limitations.
3. Develop a personal life curriculum to achieve dreams.
4. Don't wait for college or others to give you the curriculum of life.
5. Architect your own curriculum to learn new skills and improve.
6. Believe in your ability to learn and grow to achieve success.

### Question-5 What are your ideas to take action and build Growth Mindset?

- Beleave that nothign can be achieved by fear of moving forward.
- To compete with myself with yesterdays progress.
- Think of what you can rather than thinking what I am not able to do.
- Have your own timetable for the day, list out the things you have to do for the day and also what are the obstacles you will face to not follow the timetable and try to avoid the obstacles.



