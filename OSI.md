# OSI MODEL 

**The OSI (Open Systems Interconnection) model is a conceptual framework for how applications communicate over a network, consisting of seven layers. Each layer has a specific function in facilitating the transmission of signals over various protocols.**

## 7 Layers of OSI Model

1. Application Layer
2. Presentation Layer
3. Session Layer
4. Transport Layer
5. Network Layer
6. Data Link Layer
7. Physical Layer

- The **Application Layer** provides network services to end-users and includes protocols like HTTP, used by web browsers.
- The **Presentation Layer** converts data from one format to another, such as decrypting encrypted data for processing.
- The **Transport layer** coordinates the transmission of data across network connections using protocols like TCP and UDP.
- The **Network layer** handles routing and ensures data reaches its destination, managing IP addresses and addressing protocols.
- The **Data Link Layer** sets up links across the physical network, manages addressing, and checks for transmission errors.
- The **Physical Layer** encompasses the physical aspects of the network, including cables, connectors, and transmission media.

![Pictorial Representation](https://miro.medium.com/v2/resize:fit:720/format:webp/1*7yPBGx_K8GymgeGfzhfznA.gif)

## Conclusion
Understanding the OSI model helps identify the layers involved in network communication, enabling troubleshooting and selecting compatible protocols and devices.

## Reference
[Youtube][1]

[1]: https://youtu.be/vv4y_uOneC0?si=eJlOncffNy3fDukd